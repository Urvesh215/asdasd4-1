<?php

session_start();
require_once "../../api_info.php";

$name = '';
$password = '';
$credit_limit = '';

$place_bets = 0;
$status = 0;

$strike_commision = 0;
$big_ticket_intake = '';
$small_ticket_intake = '';



if($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['name'])) $name = $_POST['name'];
    if (isset($_POST['password'])) $password = $_POST['password'];
    if (isset($_POST['credit_limit'])) $credit_limit = $_POST['credit_limit'];


    $place_bets = $_POST['place_bets'];

    $status = $_POST['status'];


    $rebate = $_POST['rebate'];


    if (isset($_POST['strike_commision'])) $strike_commision = $_POST['strike_commision'];
    if (isset($_POST['big_ticket_intake'])) $big_ticket_intake = $_POST['big_ticket_intake'];
    if (isset($_POST['small_ticket_intake'])) $small_ticket_intake = $_POST['small_ticket_intake'];


    $data = array("user_name" => $_SESSION['username'], "name" => $name, "password" => $password, "credit_limit" => $credit_limit, "place_bets" => $place_bets, "status" => $status, "rebate" => $rebate, "strike_commision" => $strike_commision, "big_ticket_intake" => $big_ticket_intake, "small_ticket_intake" => $small_ticket_intake);
    $data_string = json_encode($data);

    $ch = curl_init($api_server . "createplayer");

    set_curl_opts_post($ch, $data_string);

    $result = json_decode(curl_exec($ch));
    if ($result->{'IsSucess'} == '1') {

    } else {
        echo "<script type='text/javascript'>alert('Incorrect login!');</script>";


    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
              <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
<?php require_once '../_sidebar/sidebar.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
<div class="col-md-12">
    <h2>Agent Managment</h2><br>
</div>
</div>
<div>
    <div class="col-md-12">
        <h3 style="margin-bottom: 20px;">Add Player</h3>
    </div>
</div>

    <form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

    <div>

        <div class="col-md-5">
            <div class="form-group">
                <label>User ID</label>
                <input type="text" name="username" class="form-control form-control-sm" readonly value="<?php echo $_SESSION['username']; ?>>
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control form-control-sm"">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control form-control-sm" id="password">
            </div>
            <div class="form-group">
                <label>Credit Limit</label>
                <input type="number" name="credit_limit" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Status</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" checked value="Active">Active</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" value="Lock">Lock</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" value="Terminated">Terminate</label>
                </div>
            </div>
            <div class="form-group">
                <label>Place Bets</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="place_bets" class="form-check-input" checked value="0">No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="place_bets" class="form-check-input" value="1">Yes</label>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="form-group">
                <label>4D Settings</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input" value="0" >No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input" value="1" checked>Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Big Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="1.60">
            </div>
            <div class="form-group">
                <label>Small Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="0.70">
            </div>
            <div class="form-group">
                <label>Ticket Rebate</label>
                <input type="number" step="any" name="rebate" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Strike_commission</label>
                <input type="number" step="any" name="strike_commission" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Big Ticket Intake</label>
                <input type="number" name="big_ticket_intake" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Small Ticket Intake</label>
                <input type="number" name="small_ticket_intake" class="form-control form-control-sm">
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn2">Save</button>
        </div>
    </div>
    </form>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>