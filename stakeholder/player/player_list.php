<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
              <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
                        <?php require_once '../_sidebar/sidebar.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
    <div class="col-md-5">
        <h3 class="page_title">Player Managment</h3><br>
    </div>
    <div class="col-md-7 responsive_side_align">
        <form id="search-form" onclick="return false" action="" class="form-inline" style="display: inline;">
            <label style="display: inline;">User ID/Name</label>
            <input type="text" class="form-control form-control-sm btn-sm filter" id="search-filter">
            <button id="find" type="submit" class="btn3">Find</button>
        </form>
        <button id="clear" class="btn3" style="display: inline;">View All</button>

            <button class="btn3" style="display: inline;"><a style="color: white;" href="{{ route('agent.players.create',['id' => auth('agent')->user()->id,'by' => 'agent']) }}">Add New</a></button>

    </div>
</div><br>



<div class="col-md-12">
    <h3>Player List <small>Total: {{$total}} / Active: {{$active}} / Inactive: {{$inactive}}</small></h3><br>
</div>

<div class="col-md-12">
    <div class="col-md-12 pl-0">
    <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Credit</th>
                <th>Rebate(%)</th>
                <th>Active Bets</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                    <td>1</td>
                    <th scope="row">EA</th>
                    <td>Jerry</td>
                    <td>2000</td>
                    <td>9.00</td>
                    <td>9.00</td>
            </tr>
        </tbody>    
    </table>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
<script type="text/javascript">
   $(document).ready(function(){
        oTable = $('#stakeholders').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "pageLength": 100,
            "ajax": {
                        @if(auth('admin')->check())
                            url:"{{ route('admin.players.data') }}",
                        @elseif(auth('stakeholder')->check())
                            url:"{{ route('stakeholder.players.data') }}",
                        @elseif(auth('agent')->check())
                            url:"{{ route('agent.players.data') }}",
                        @endif
                        method: 'get',
                        data: function(d)
                        {
                            d.search =  $('#search-filter').val();
                        }
                    },
            responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
        "dom": '<"top">rt<"table_footer"<"col-md-6 px-0"p>><"clear">',
            "columns": [
                {data: 'username', name: 'username', searchable: true},
                {data: 'name', name: 'name', searchable: true},
                {data: 'credit_limit', name: 'credit_limit', searchable: false},
                {data: 'rebate', name: 'rebate', searchable: false},
                {data: 'active_bets', name: 'active_bets', searchable: false},
                {data: 'upline', name: 'upline', searchable: false},
            ],
        });
    });

   searchUsernameAndName();
</script>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>