1. Da se povrzat statichki linkovite od php fajlovite
   - primer u agent/agent/agent_list.php ima nav bar (ima u site navbar, toj od levo sho e, ama fajlot go zeam kako primer) i treba da se povrzi da ne stojat prazni ovie fields:
   
                                <li><a href="#">full report</a></li>
                                <li><a href="#">stake report</a></li>
                                <li><a href="#">strike report</a></li>
                                <li><a href="#">summary report</a></li>
								
	Kao sho gleash, ima staveno '#' a treba da e primer za full report href="../reports/full_report.php" 
	U admin se sredeni site, mozhesh da gi vidish tamu, sea samo treba da se sredat u stakeholder, agent i player
	
2. U place_bets.php, go ima u site vidovi na playeri - agent, player etc., ima 60 polinja za vnesuvanje na bets. Jas imam napraveno nekojsi forlup za da gi iteriram site input types ama ne sum siguren dali raboti i go nemam ni testirano, a i kopchinjata so formite ne mi se napraveni kako sho treba poshto u toj del od php sum epten slab. Ako mozhesh da gi definirash barem funkciite za buttonite toa bi bilo ekstra isto taka.

3. U adm/reports/full_report.php, ili bilo koj drug full_report od drugite tipovi na accounts, ima tabeli sho se statichki napraveno momentalno. Dali imash ideja, ako primer vrakjame od apito json sho mozhe da se parsira, kako mozhe da ja generirame tabelata so podatoci? I dali ke e moguche da se napraat fajl/link generatori za sekoj generiran podatok? Kako sho e u vegas, primer reportot ti gi vrakja site stakeholderi, KUR, MUR, SUR, i sea ako kliknesh na KUR ti go otvara negoviot lichen report, toa e kako generiran link valjda, eden string so get call i samo kako id da se dodava userot ifaktichki eden fajl za site? 

Ako ti trebaat api calls zirni u backendot, ima za sekoe chudo sho ke ti pritreba.

Primer api calls:

Za login: 

curl -H "Content-Type: application/json" -X POST -d '{"username":"test", "password":"test"}' http://18.195.247.159:52817/api/login

Za expenses:

curl -H "Content-Type: application/json"http://18.195.247.159:52817/api/expenses

Za updatiranje na expenses:

curl -H "Content-Type: application/json" -X POST -d '{"description":"testtest","amount":"500"}' http://18.195.247.159:52817/api/expenses

Za kreiranje agent:

curl -H "Content-Type: application/json" -X POST -d '{"user_name":"testasd","name":"testasd","password":"kur","credit_limit","5000",....' http://18.195.247.159:52817/api/createagent
	