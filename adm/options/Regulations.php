<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div id="clock"></div>
            <p id="date"></p>
            <div class="divbtn"><button class="btn1" ">Logout</button></div>
            <div class="logo"><a href="#">LOGO</a></div>
            <ul>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i> Admin <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../admin/profile.php">PROFILE</a></li>
                        <li><a href="../admin/company.php">COMPANY</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../admin/expenses.php">EXPENSES</a></li>
                        <li><a href="../stakeholder/stakeholder_list.php">STAKEHOLDER</a></li>
                        <li><a href="../group/group_list.php">GROUP</a></li>
                        <li><a href="../agent/agent_list.php">AGENT</a></li>
                        <li><a href="../player/player_list.php">PLAYER</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../intake/intake_acc_list.php">INTAKE account</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../outset/outset_list.php">OUTSET</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../reports/full_report.php">RESULT</a></li>
                        <li><a href="../common/payment.php">PAYMENT</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../common/payment_history.php">payment HISTORY</a></li>
                        <li><a href="../admin/change_password.php">PASSWORD</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> Bets <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../Bets/place_bets.php">PLACE BETS</a></li>
                        <li><a href="../Bets/uploud_bets.php">Upload bets</a></li>
                        <li ><a href="../Bets/fixed_bets.php">Fixed bets</a></li>
                        <li><a href="../Bets/Out_Bets.php">out bets</a></li>
                        <li><a href="../Bets/Process_outset.php">process outset</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../Bets/summary_bets.php">summary bets</a></li>
                        <li><a href="../Bets/View_tickets.php">view tickets</a></li>
                        <li><a href="../Bets/Search_tickets.php">search tickets</a></li>
                        <li><a href="../Bets/History_tickets.php">history</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-receipt"></i>Reports <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../reports/full_report.php">full report</a></li>
                        <li><a href="../reports/Group_report.php">group report</a></li>
                        <li><a href="../reports/Stake_report.php">stake report</a></li>
                        <li><a href="../reports/Strike_report.php">strike report</a></li>
                        <li><a href="../reports/Summary_report.php">summary report</a></li>



                    </ul>
                </li>

                <li class="open"><a class="sidebar-sub-toggle"><i class="ti-help-alt"></i> Options <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li style="border-bottom: 1px solid black;"><a href="../options/Message.php">message</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../options/announcement.php">announcment</a></li>
                        <li><a href="../options/Regulations.php">regulations</a></li>



                    </ul>
                </li>


            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.bwyn138.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
    <div class="col-md-12">
        <h1>Regulations</h1>
    </div><br>
    <div class="col-md-12">
        <h3>Bwyn138.com system payout</h3>
        <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
            <tr>
                <th>Prizes</th>
                <th>Big</th>
                <th>Small</th>
            </tr>
            <tr>
                <td>1st Prize *</td>
                <td>4000$</td>
                <td>3000$</td>
            </tr>
            <tr>
                <td>2nd Prize *</td>
                <td>2000$</td>
                <td>2000$</td>
            </tr>
            <tr>
                <td>3rd Prize *</td>
                <td>1000$</td>
                <td>1000$</td>
            </tr>
            <tr>
                <td>10 Starters *</td>
                <td>500$ each</td>
                <td>-</td>
            </tr>
            <tr>
                <td>10 Consolations *</td>
                <td>150$ each</td>
                <td>-</td>
            </tr>
        </table>
    </div><br><br/><br/>
    <div class="col-md-12">
        <h3>I-bet system payout</h3>
        <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
            <tr>
                <th>Prizes</th>
                <th>Big</th>
                <th></th>
                <th></th>
                <th>Small</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <th>24 roll</th>
                <th>12 roll</th>
                <th>6 roll</th>
                <th>4 roll</th>
                <th>24 roll</th>
                <th>12 roll</th>
                <th>6 roll</th>
                <th>4 roll</th>
            </tr>
            <tr>
                <td>1st Prize *</td>
                <td>166$</td>
                <td>333$</td>
                <td>666$</td>
                <td>1000$</td>
                <td>125$</td>
                <td>250$</td>
                <td>500$</td>
                <td>750$</td>
            </tr>
            <tr>
                <td>2nd Prize *</td>
                <td>83$</td>
                <td>166$</td>
                <td>333$</td>
                <td>500$</td>
                <td>83$</td>
                <td>166$</td>
                <td>333$</td>
                <td>500$</td>
            </tr>
            <tr>
                <td>3rd Prize *</td>
                <td>41$</td>
                <td>83$</td>
                <td>166$</td>
                <td>250$</td>
                <td>41$</td>
                <td>83$</td>
                <td>166$</td>
                <td>250$</td>
            </tr>
            <tr>
                <td>Starter *</td>
                <td>20$</td>
                <td>41$</td> <td>83</td>
                <td>125$</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Consolation *</td>
                <td>6$</td>
                <td>12$</td>
                <td>25$</td>
                <td>37$</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <p>* Commissions not deducted</p>
        <br/>
    </div>
    <div class="col-md-12">
        <p><strong style="font-size: 15px;">General Rules</strong><br>
        Players must be at least 18 years of age. bwyn138.com is not responsible for any players using our website. bwyn138.com absolutely holds no responsibility for any national laws, federal laws or international laws infringed by any players using our website. Players are responsible for their own action for that matter and to bind by their own countries laws, whether it involved with any states’ laws or legal issues. Any attempts of fraud will render your account void. bwyn138.com will not pay the prizes or winnings to which a player might otherwise be entitled if their payment is dishonored in anyway and for whatsoever reason. We reserve the right to cancel all outstanding entries for such players and prevent them from playing at our own discretion. We also reserve the right to refuse entries. All Agents are responsible for their respective members’ accounts and have to honor all bets submitted by them. Agents are not partner of bwyn138.com. Any discrepancies that arise between Players and Agents are not the responsibility of bwyn138.com. bwyn138.com will not responsible for any stolen passwords due to players or agents own negligence or any other party involvement. All players are responsible for their own accounts and any transactions exchanged through their accounts. By way of log-in and placing your bet, you hereby agreed with our Rules and Regulations and declared that you are of legal age to participate in the betting and also agreed that bwyn138.com will not hold any responsibilities or any liabilities with any legal issues that you as a Player have gotten involved into.</p><br>
        <p><strong style="font-size: 15px;">Gaming</strong><br>
        Red Numbers are numbers that are full in our data base and are paid acconding to market rates. bwyn138.com do not guaranteed any minimum  payout for Red Number. bwyn138.com has the final decision and it will be deem final. There shall be no further claims from either parties thereafter. We do not entertain any change of bets, requests for refund of bets or any other objections that may arise due to Players negligence or errors during the course of submission. This website shall not be construed to form any part, whether express or implied, of the terms and condition of the Games.</p>
        <p><strong style="font-size: 15px;">Limits and Liabilities</strong><br>
        The decision of bwyn138.com will be final and binding in all matters and no correspondence will be entered into. bwyn138.com will not be liable for any loss of whatever nature arising from the cancellation of any matches or draws, including the loss for whatsoever reason whether the chance to participate in a bet. Without prejudice to the generality of the foregoing, bwyn138.com will not be liable to any person in the event of force majeure, and or for the events of failure or damage or destruction to bwyn138.com central computer system or records or any part thereof, or delays, losses, errors, omissions or any others thereof resulting from failure of any telecommunications or any other data transmission system, or any delay resulting in non receipt of any entry for a particular bet or draw. bwyn138.com will not be liable for any loss as a result of an act of God, in an outbreak of hostilities, riot, civil unrest, act of terrorism, act of any government or authority (including refusal or revocation of any license or consent), fire explosion, flood, theft, malicious damage, strike, lock-out and industrial action of any kind. The Parties shall not commit nor purport to commit the other to honor any obligation other than is specifically provided for by these Rules and Regulations. bwyn138.com reserves the right to amend these Rules and Regulations from time to time and vary any of them without prior notice.</p>
        <p><strong style="font-size: 15px;">Discrepancy</strong><br>
        In the event there is discrepancy between the bets that the Player claimed and the data base of bwyn138.com, those in the data base maintained by bwyn138.com will prevail and considered final and valid. The responsibility for entries, submit, confirmed and acceptance for any game bets lay in the hand of the Players at all time. Bets submitted after the closing time will be deem null and void unless specified. These Rules and Regulations constitute the entire agreement and understanding between the parties. If there is any discrepancy between English language version of these Rules and Regulations and any other language version the English language version supercede the other languages and is deem correct and valid.</p>
        <p><strong style="font-size: 15px;">Disclaimer</strong><br>
        Whilst every effort is made to ensure the accuracy of all information published at this site. bwyn138.com asserts that it would not be responsible for whatsoever cause by any inaccuracies of it published results, errors, omissions, misleading, out of date information or any others distortion. bwyn138.com is not in any way associated to any government body or private companies, committees, institutes or organizations dueling with lottery draw. 
</p>
    </div>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>